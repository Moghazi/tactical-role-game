﻿using Unity.Entities;
using Unity.Mathematics;

public struct UpdateHighlightPosition : IComponentData{
     public float3 newPosition;
}
