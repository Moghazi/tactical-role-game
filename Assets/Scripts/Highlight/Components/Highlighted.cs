﻿using Unity.Entities;
using Unity.Mathematics;

public struct Highlighted : IComponentData
{
    public float3 position;
}
