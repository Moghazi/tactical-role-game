﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Mathematics;
using Units.Systems;

public class HighlightSystem : ComponentSystem
{
    private const string quadString = "Quad";
    private const string highlightMaterial = "HighlightMaterial";
    private EntityQuery highlightEntityQuery;
    private EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
    private Mesh mesh;
    private Material material;
    private Entity highlightEntity;

    protected override void OnCreate()
    {
        InitHighlightEntity();
    }

    protected override void OnUpdate()
    {
        Entities.WithAll<Highlighted>().ForEach((Entity entity, ref Highlighted newPosition) =>
        {
            ShowHighlight(newPosition.position);
            entityManager.RemoveComponent(entity, typeof(Highlighted));
        });
    }

    private void InitHighlightEntity()
    {
        mesh = ((GameObject)Resources.Load(quadString)).GetComponent<MeshFilter>().sharedMesh;
        material = Resources.Load<Material>(highlightMaterial);

        var highlightArchetype = entityManager.CreateArchetype(
            typeof(Highlight),
            typeof(Translation),
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(RenderBounds)
            );

        highlightEntity = entityManager.CreateEntity(highlightArchetype);

        highlightEntityQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[] {
                ComponentType.ReadOnly<Highlight>(),
            }
        });
    }

    private void ShowHighlight(float3 pos)
    {

        if (!entityManager.HasComponent(highlightEntity, typeof(RenderMesh)))
        {
            entityManager.AddComponent(highlightEntity, typeof(RenderMesh));
        }

        entityManager.SetComponentData(highlightEntity, new Translation { Value = new float3(pos.x, pos.y, 1f) });

        entityManager.SetSharedComponentData(highlightEntity, new RenderMesh
        {
            mesh = mesh,
            material = material
        });

        entityManager.SetComponentData(highlightEntity, new RenderBounds
        {
            Value = mesh.bounds.ToAABB(),
        });
    }

    private void HideHighlight()
    {
        if (entityManager.HasComponent(highlightEntity, typeof(RenderMesh)))
        {
            entityManager.RemoveComponent(highlightEntity, typeof(RenderMesh));
        }
    }
}