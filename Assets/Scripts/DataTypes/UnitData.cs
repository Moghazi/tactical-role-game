﻿using Unity.Mathematics;
using System;
using UnityEngine;

[Serializable]
public class UnitData
{
    public float2 UnitPosition;
    public int Health;
    public int Attack;
    public Mesh Mesh;
    public Material Material;
}
