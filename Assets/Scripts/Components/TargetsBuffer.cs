﻿using Unity.Entities;
using Unity.Mathematics;

[InternalBufferCapacity(20)]
public struct TargetsBuffer : IBufferElementData
{
    public float3 position;
    public Entity entity;
}