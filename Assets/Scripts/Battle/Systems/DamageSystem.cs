﻿using Unity.Entities;
using UnityEngine;

public class DamageSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.WithAll<TakeDamage>().ForEach((Entity entity, ref TakeDamage takeDamege, ref Health health) =>
        {
            health.Value -= takeDamege.amount;

            if (health.Value <= 0)
            {
                Debug.Log("Unit " + entity.Index + " is Dead...");
                PostUpdateCommands.DestroyEntity(entity);
            }
            else
            {
                PostUpdateCommands.RemoveComponent(entity, typeof(TakeDamage));
            }
        });
    }
}