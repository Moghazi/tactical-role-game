﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class AIBattleSystem : ComponentSystem
{
    public EntityCommandBuffer.Concurrent entityCommandBuffer;

    protected override void OnUpdate()
    {
        Entities.WithAll<AI, InAction, WaitingForBattle>().ForEach((Entity entity, DynamicBuffer<TargetsBuffer> targetsBuffer, ref Translation translation, ref Attack attack) =>
        {
            PostUpdateCommands.AddComponent(targetsBuffer[0].entity, typeof(TakeDamage));
            PostUpdateCommands.SetComponent(targetsBuffer[0].entity, new TakeDamage { amount = attack.Value });
            PostUpdateCommands.AddComponent<EndTurn>(entity);
            PostUpdateCommands.RemoveComponent(entity, typeof(WaitingForBattle));
            Debug.Log("Player Damage " + attack.Value);

        });
    }

    public int GetEntityIndexByPostion(float3 position, NativeArray<Entity> array)
    {
        int entityIndex = -1;
        for (int i = 0, arrayCount = array.Length; i < arrayCount; i++)
        {
            Translation translation = EntityManager.GetComponentData<Translation>(array[i]);

            if (math.distance(position, translation.Value) <= 0)
            {
                entityIndex = i;
            }
        }

        return entityIndex;
    }
}
