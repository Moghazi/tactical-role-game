﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class SearchForBattlesSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.WithAll<InAction, SearchingForBattle>().ForEach((Entity entity, DynamicBuffer<TargetsBuffer> targetBuffer, ref Translation translation) =>
        {
            var targetQuery = GetTargetQuery(entity);
            var targetsPositionEntityArray = targetQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
            var targetsEntityArray = targetQuery.ToEntityArray(Allocator.TempJob);

            targetBuffer.Clear();

            for (int i = 0; i < targetsPositionEntityArray.Length; i++)
            {
                if (GridUtilities.IsTargetNeighbour(translation.Value, targetsPositionEntityArray[i].Value))
                {
                    targetBuffer.Add(new TargetsBuffer
                    {
                        position = targetsPositionEntityArray[i].Value,
                        entity = targetsEntityArray[i]
                    });
                }
            }

            if (targetBuffer.Length > 0)
            {
                PostUpdateCommands.AddComponent<WaitingForBattle>(entity);
                Debug.Log(targetBuffer.Length + " battle found. Tap on unit to attack.");
            }
            else
            {
                PostUpdateCommands.AddComponent<EndTurn>(entity);
            }

            EntityManager.RemoveComponent<SearchingForBattle>(entity);

            targetsPositionEntityArray.Dispose();
            targetsEntityArray.Dispose();
        });
    }

    private EntityQuery GetTargetQuery(Entity entity)
    {
        EntityQuery targetQuery;
        if (EntityManager.HasComponent(entity, typeof(Player)))
        {
            targetQuery = GetEntityQuery(typeof(AI), ComponentType.ReadOnly<Translation>());
        }
        else
        {
            targetQuery = GetEntityQuery(typeof(Player), ComponentType.ReadOnly<Translation>());
        }

        return targetQuery;
    }
}