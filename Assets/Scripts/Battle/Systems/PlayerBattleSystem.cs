﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class PlayerBattleSystem : ComponentSystem
{
    public EntityCommandBuffer.Concurrent entityCommandBuffer;

    protected override void OnUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var mousePosition = InputUtilities.GetMouseWorldPosition();
            GridUtilities.ConvertToGridPosition(mousePosition, out int clickPositionX, out int clickPositionY);
            Entities.WithAll<Player, InAction>().ForEach((Entity entity, DynamicBuffer<TargetsBuffer> targetsBuffer, ref Translation translation, ref Attack attack) =>
            {
                GridUtilities.ConvertToGridPosition(translation.Value, out int playerPositionX, out int playerPositionY);
                CheckForAttack(targetsBuffer, new int2(playerPositionX, playerPositionY), new int2(clickPositionX, clickPositionY));
                PerformAttack(entity, targetsBuffer, attack, clickPositionX, clickPositionY);
            });
        }
    }

    private void CheckForAttack(DynamicBuffer<TargetsBuffer> targetsBuffer, int2 playerPosition, int2 mousePosition)
    {
        var neighbourOffsetArray = GridUtilities.GetNeigbourOffsetArray();
        var entityQuery = GetEntityQuery(typeof(AI), typeof(Translation));
        var AIEntitiesArray = entityQuery.ToEntityArray(Allocator.TempJob);
        var AIEntitiesPistionArray = entityQuery.ToComponentDataArray<Translation>(Allocator.TempJob);

        for (int i = 0; i < neighbourOffsetArray.Length; i++)
        {
            int2 neighbourOffset = neighbourOffsetArray[i];
            int2 neighbourPosition = new int2(playerPosition.x + neighbourOffset.x, playerPosition.y + neighbourOffset.y);

            if (neighbourPosition.x == mousePosition.x && neighbourPosition.y == mousePosition.y)
            {
                for (int j = 0; j < AIEntitiesArray.Length; j++)
                {
                    GridUtilities.ConvertToGridPosition(AIEntitiesPistionArray[j].Value, out int unitPositionX, out int unitPositionY);
                    var TargetsBuffer = new TargetsBuffer { entity = AIEntitiesArray[j], position = AIEntitiesPistionArray[j].Value };
                    targetsBuffer.Add(TargetsBuffer);
                }
            }
        }
        AIEntitiesArray.Dispose();
        AIEntitiesPistionArray.Dispose();
        neighbourOffsetArray.Dispose();
    }

    private void PerformAttack(Entity entity, DynamicBuffer<TargetsBuffer> targetsBuffer, Attack attack, int clickPositionX, int clickPositionY)
    {
        for (int i = 0; i < targetsBuffer.Length; i++)
        {
            if (!EntityManager.Exists(targetsBuffer[i].entity))
            {
                continue;
            }

            GridUtilities.ConvertToGridPosition(targetsBuffer[i].position, out int targetPositionX, out int targetPositionY);

            if (clickPositionX == targetPositionX && clickPositionY == targetPositionY)
            {
                PostUpdateCommands.AddComponent(targetsBuffer[i].entity, typeof(TakeDamage));
                PostUpdateCommands.SetComponent(targetsBuffer[i].entity, new TakeDamage { amount = attack.Value });
                PostUpdateCommands.AddComponent<EndTurn>(entity);
                Debug.Log("Enemy Damage " + attack.Value);
            }
        }
    }
}