﻿using Unity.Entities;

public struct TakeDamage : IComponentData
{
    public int amount;
}
