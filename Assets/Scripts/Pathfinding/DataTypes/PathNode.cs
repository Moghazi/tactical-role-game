﻿using Unity.Mathematics;

public struct PathNode
{
    public int2 position;
    public int index;
    public int gCost;
    public int hCost;
    public int fCost;
    public bool isAvalible;
    public int previousNodeIndex;

    public void CalculateFCost()
    {
        fCost = gCost + hCost;
    }
}
