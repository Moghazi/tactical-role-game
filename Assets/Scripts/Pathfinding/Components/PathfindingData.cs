﻿using Unity.Entities;
using Unity.Mathematics;

public struct PathfindingData : IComponentData
{
    public int2 startPosition;
    public int2 targetPosition;
}
