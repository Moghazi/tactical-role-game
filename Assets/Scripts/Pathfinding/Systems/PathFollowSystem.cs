﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class PathFollowSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.WithAll<PathFollow>().WithNone<ReachedPathTarget>().ForEach((Entity entity, DynamicBuffer<PathPosition> pathPositionBuffer, ref Translation translation, ref PathFollow pathFollow) =>
        {
            if (pathFollow.pathIndex >= 0)
            {
                var pathPosition = pathPositionBuffer[pathFollow.pathIndex].position;
                var targetPosition = new float3(pathPosition.x, pathPosition.y, 0);
                var moveDir = math.normalizesafe(targetPosition - translation.Value);
                var moveSpeed = 5f;
                translation.Value += moveDir * moveSpeed * Time.DeltaTime;

                if (math.distance(translation.Value, targetPosition) < .1f)
                {
                    // Next waypoint
                    translation.Value = targetPosition;
                    pathFollow.pathIndex--;
                }
            }
            else
            {
                EntityManager.AddComponent(entity, typeof(ReachedPathTarget));
                PostUpdateCommands.AddComponent(entity, typeof(SearchingForBattle));
            }
        });
    }
}