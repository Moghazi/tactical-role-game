﻿using UnityEngine;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Unity.Burst;
using Unity.Entities;
using System.Collections.Generic;

public class Pathfinding : ComponentSystem
{
    protected override void OnUpdate()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var girdEntityQuary = entityManager.CreateEntityQuery(ComponentType.ReadOnly<Grid>());
        var grid = girdEntityQuary.GetSingleton<Grid>();
        var findPathJobList = new List<FindPathJob>();
        var jobHandleList = new NativeList<JobHandle>(Allocator.Temp);
        var pathNodeArray = InitPathNodeArray(grid);

        Entities.WithAll<PathFollow>().ForEach((Entity entity, ref PathfindingData pathfindingData) =>
        {
            var tmpPathNodeArray = new NativeArray<PathNode>(pathNodeArray, Allocator.TempJob);

            FindPathJob findPathJob = new FindPathJob
            {
                gridSize = new int2(grid.width, grid.height),
                pathNodeArray = tmpPathNodeArray,
                startPosition = pathfindingData.startPosition,
                targetPosition = pathfindingData.targetPosition,
                entity = entity
            };

            findPathJobList.Add(findPathJob);
            jobHandleList.Add(findPathJob.Schedule());

            PostUpdateCommands.RemoveComponent<PathfindingData>(entity);
        });

        JobHandle.CompleteAll(jobHandleList);

        foreach (FindPathJob findPathJob in findPathJobList)
        {
            new SetBufferPathJob
            {
                entity = findPathJob.entity,
                gridSize = findPathJob.gridSize,
                pathNodeArray = findPathJob.pathNodeArray,
                pathfindingDataComponentDataFromEntity = GetComponentDataFromEntity<PathfindingData>(),
                pathFollowComponentDataFromEntity = GetComponentDataFromEntity<PathFollow>(),
                pathPositionBufferFromEntity = GetBufferFromEntity<PathPosition>(),
            }.Run();
        }

        pathNodeArray.Dispose();
    }

    private NativeArray<PathNode> InitPathNodeArray(Grid grid)
    {
        ref var tilesArray = ref grid.gridBlobAssetRef.Value.tilesBlobArray;
        var pathNodeArray = new NativeArray<PathNode>(grid.width * grid.height, Allocator.Temp);

        for (int x = 0; x < grid.width; x++)
        {
            for (int y = 0; y < grid.height; y++)
            {
                var index = GetNodeIndex(new int2(x, y), grid.width);

                PathNode pathNode = new PathNode
                {
                    position = new int2(x, y),
                    index = index,
                    gCost = int.MaxValue,
                    isAvalible = tilesArray[index].isAvailable,
                    previousNodeIndex = -1
                };

                pathNodeArray[pathNode.index] = pathNode;
            }
        }

        return pathNodeArray;
    }

    [BurstCompile]
    private struct SetBufferPathJob : IJob
    {
        public int2 gridSize;

        [DeallocateOnJobCompletion]
        public NativeArray<PathNode> pathNodeArray;

        public Entity entity;

        public ComponentDataFromEntity<PathfindingData> pathfindingDataComponentDataFromEntity;
        public ComponentDataFromEntity<PathFollow> pathFollowComponentDataFromEntity;
        public BufferFromEntity<PathPosition> pathPositionBufferFromEntity;

        public void Execute()
        {
            DynamicBuffer<PathPosition> pathPositionBuffer = pathPositionBufferFromEntity[entity];
            pathPositionBuffer.Clear();

            PathfindingData pathfindingData = pathfindingDataComponentDataFromEntity[entity];
            int targetNodeIndex = GetNodeIndex(pathfindingData.targetPosition, gridSize.x);
            PathNode targetNode = pathNodeArray[targetNodeIndex];
            if (targetNode.previousNodeIndex == -1)
            {
                // Didn't find a path!
                //Debug.Log("Didn't find a path!");
                pathFollowComponentDataFromEntity[entity] = new PathFollow { pathIndex = -1 };
            }
            else
            {
                // Found a path
                CalculatePath(pathNodeArray, targetNode, pathPositionBuffer);
                pathFollowComponentDataFromEntity[entity] = new PathFollow { pathIndex = pathPositionBuffer.Length - 1 };
            }
        }
    }

    [BurstCompile]
    private struct FindPathJob : IJob
    {
        public int2 gridSize;
        public NativeArray<PathNode> pathNodeArray;

        public int2 startPosition;
        public int2 targetPosition;

        public Entity entity;

        public void Execute()
        {
            for (int i = 0; i < pathNodeArray.Length; i++)
            {
                PathNode pathNode = pathNodeArray[i];
                pathNode.hCost = MathUtilities.CalculateHeuristicCost(new int2(pathNode.position.x, pathNode.position.y), targetPosition);
                pathNode.previousNodeIndex = -1;

                pathNodeArray[i] = pathNode;
            }

            NativeArray<int2> neighbourOffsetArray = GridUtilities.GetNeigbourOffsetArray();

            int targetNodeIndex = GetNodeIndex(targetPosition, gridSize.x);

            PathNode startNode = pathNodeArray[GetNodeIndex(startPosition, gridSize.x)];
            startNode.gCost = 0;
            startNode.CalculateFCost();
            pathNodeArray[startNode.index] = startNode;

            NativeList<int> openList = new NativeList<int>(Allocator.Temp);
            NativeList<int> closedList = new NativeList<int>(Allocator.Temp);

            openList.Add(startNode.index);

            while (openList.Length > 0)
            {
                int currentNodeIndex = GetLowestCostFNodeIndex(openList, pathNodeArray);
                PathNode currentNode = pathNodeArray[currentNodeIndex];

                if (currentNodeIndex == targetNodeIndex)
                {
                    // Reached our destination!
                    break;
                }

                // Remove current node from Open List
                for (int i = 0; i < openList.Length; i++)
                {
                    if (openList[i] == currentNodeIndex)
                    {
                        openList.RemoveAtSwapBack(i);
                        break;
                    }
                }

                closedList.Add(currentNodeIndex);

                for (int i = 0; i < neighbourOffsetArray.Length; i++)
                {
                    int2 neighbourOffset = neighbourOffsetArray[i];
                    int2 neighbourPosition = new int2(currentNode.position.x + neighbourOffset.x, currentNode.position.y + neighbourOffset.y);

                    if (!IsNodePositionInsideGrid(neighbourPosition, gridSize))
                    {
                        // Neighbour not valid position
                        continue;
                    }

                    int neighbourNodeIndex = GetNodeIndex(neighbourPosition, gridSize.x);

                    if (closedList.Contains(neighbourNodeIndex))
                    {
                        // Already searched this node
                        continue;
                    }

                    PathNode neighbourNode = pathNodeArray[neighbourNodeIndex];
                    if (!neighbourNode.isAvalible)
                    {
                        // Not availible
                        continue;
                    }

                    int2 currentNodePosition = new int2(currentNode.position.x, currentNode.position.y);

                    int tentativeGCost = currentNode.gCost + MathUtilities.CalculateHeuristicCost(currentNodePosition, neighbourPosition);
                    if (tentativeGCost < neighbourNode.gCost)
                    {
                        neighbourNode.previousNodeIndex = currentNodeIndex;
                        neighbourNode.gCost = tentativeGCost;
                        neighbourNode.CalculateFCost();
                        pathNodeArray[neighbourNodeIndex] = neighbourNode;

                        if (!openList.Contains(neighbourNode.index))
                        {
                            openList.Add(neighbourNode.index);
                        }
                    }
                }

            }

            neighbourOffsetArray.Dispose();
            openList.Dispose();
            closedList.Dispose();
        }
    }

    private static void CalculatePath(NativeArray<PathNode> pathNodeArray, PathNode targetNode, DynamicBuffer<PathPosition> pathPositionBuffer)
    {
        if (targetNode.previousNodeIndex == -1)
        {
            // Couldn't find a path!
        }
        else
        {
            // path Found
            pathPositionBuffer.Add(new PathPosition { position = new int2(targetNode.position.x, targetNode.position.y) });

            PathNode currentNode = targetNode;
            while (currentNode.previousNodeIndex != -1)
            {
                PathNode previousNode = pathNodeArray[currentNode.previousNodeIndex];
                pathPositionBuffer.Add(new PathPosition { position = new int2(previousNode.position.x, previousNode.position.y) });
                currentNode = previousNode;
            }
        }
    }

    private static bool IsNodePositionInsideGrid(int2 nodePosition, int2 gridSize)
    {
        return nodePosition.x >= 0 &&
               nodePosition.y >= 0 &&
               nodePosition.x < gridSize.x &&
               nodePosition.y < gridSize.y;
    }

    private static int GetNodeIndex(int2 position, int gridWidth)
    {
        return position.x + position.y * gridWidth;
    }

    private static int GetLowestCostFNodeIndex(NativeList<int> openList, NativeArray<PathNode> pathNodeArray)
    {
        PathNode lowestCostPathNode = pathNodeArray[openList[0]];
        for (int i = 1; i < openList.Length; i++)
        {
            PathNode testPathNode = pathNodeArray[openList[i]];
            if (testPathNode.fCost < lowestCostPathNode.fCost)
            {
                lowestCostPathNode = testPathNode;
            }
        }
        return lowestCostPathNode.index;
    }
}