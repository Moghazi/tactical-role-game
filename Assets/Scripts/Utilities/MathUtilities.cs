﻿using Unity.Mathematics;

public static class MathUtilities
{
    private const int MOVE_STRAIGHT_COST = 10;
    private const int MOVE_DIAGONAL_COST = 14;

    /// <summary>
    /// Calculates the heuristic cost. this method using the Diagonal distance heuristic.
    /// Refrence Link http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html
    /// </summary>
    /// <returns>The heuristic cost.</returns>
    /// <param name="node">Node.</param>
    /// <param name="goal">Goal.</param>
    public static int CalculateHeuristicCost(int2 node, int2 goal)
    {
        int distanceX = math.abs(node.x - goal.x);
        int distanceY = math.abs(node.y - goal.y);
        int StraightDistance = math.abs(distanceX - distanceY);
        return MOVE_STRAIGHT_COST * StraightDistance + MOVE_DIAGONAL_COST * math.min(distanceX, distanceY);
    }
}