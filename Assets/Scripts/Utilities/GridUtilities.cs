﻿using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

public static class GridUtilities
{
    public static NativeArray<int2> GetNeigbourOffsetArray()
    {
        NativeArray<int2> neighbourOffsetArray = new NativeArray<int2>(8, Allocator.Temp);
        neighbourOffsetArray[0] = new int2(-1, 0); // Left
        neighbourOffsetArray[1] = new int2(+1, 0); // Right
        neighbourOffsetArray[2] = new int2(0, +1); // Up
        neighbourOffsetArray[3] = new int2(0, -1); // Down
        neighbourOffsetArray[4] = new int2(-1, -1); // Left Down
        neighbourOffsetArray[5] = new int2(-1, +1); // Left Up
        neighbourOffsetArray[6] = new int2(+1, -1); // Right Down
        neighbourOffsetArray[7] = new int2(+1, +1); // Right Up
        return neighbourOffsetArray;
    }

    public static bool IsTargetNeighbour(float3 unitA, float3 unitB)
    {
        float StraightCost = 1;
        float DiagonalCost = 1.4f;
        float distanceX = Mathf.Abs(unitA.x - unitB.x);
        float distanceY = Mathf.Abs(unitA.y - unitB.y);
        float StraightDistance = Mathf.Abs(distanceX - distanceY);
        return StraightDistance * StraightCost + DiagonalCost * Mathf.Min(distanceX, distanceY) <= 1.4f;
    }

    public static void ConvertToGridPosition(float3 worldPosition, out int x, out int y)
    {
        x = Mathf.FloorToInt(worldPosition.x);
        y = Mathf.FloorToInt(worldPosition.y);
    }
}