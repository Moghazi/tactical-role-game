﻿using Unity.Mathematics;
using UnityEngine;

public static class InputUtilities
{
    public static float3 GetMouseWorldPosition()
    {
        float3 touchPosistion = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        touchPosistion += 0.5f;
        touchPosistion.z = 0f;
        return touchPosistion;
    }
}