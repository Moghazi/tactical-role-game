﻿using Unity.Entities;

public struct GridBlobAsset
{
    public BlobArray<Tile> tilesBlobArray;
}