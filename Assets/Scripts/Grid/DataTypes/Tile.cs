﻿using Unity.Mathematics;

public struct Tile
{
    public int2 position;
    public bool isAvailable;
}