﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class GridVisualizer : MonoBehaviour
{
    [SerializeField]
    private Mesh tileMesh;
    [SerializeField]
    private Material tileMaterial;

    void Start()
    {
        DrawGrid();
    }

    private void DrawGrid()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var tileArchetype = entityManager.CreateArchetype(
             typeof(Translation),
             typeof(TileView),
             typeof(RenderMesh),
             typeof(LocalToWorld),
             typeof(RenderBounds)
        );

        var girdEntityQuary = entityManager.CreateEntityQuery(ComponentType.ReadOnly<Grid>());
        var grid = girdEntityQuary.GetSingleton<Grid>();
        ref var tilesArray = ref grid.gridBlobAssetRef.Value.tilesBlobArray;
        var entitytilesArray = new NativeArray<Entity>(tilesArray.Length, Allocator.Temp);
        entityManager.CreateEntity(tileArchetype, entitytilesArray);

        for (int i = 0; i < entitytilesArray.Length; i++)
        {
            ref var pos = ref tilesArray[i].position;

            Entity tileEntity = entitytilesArray[i];
            entityManager.SetComponentData(tileEntity, new TileView { isAvailable = tilesArray[i].isAvailable });
            entityManager.SetComponentData(tileEntity, new Translation { Value = new float3(pos.x, pos.y, 2f) });
            entityManager.SetSharedComponentData(tileEntity, new RenderMesh
            {
                mesh = tileMesh,
                material = tileMaterial
            });

            entityManager.SetComponentData(tileEntity, new RenderBounds
            {
                Value = tileMesh.bounds.ToAABB(),
            });
        }

        entitytilesArray.Dispose();
    }
}