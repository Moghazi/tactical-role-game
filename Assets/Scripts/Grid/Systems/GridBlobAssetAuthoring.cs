﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class GridBlobAssetAuthoring : MonoBehaviour {
    [SerializeField]
    private int width = 10;
    [SerializeField]
    private int height = 10;

    private void Awake()
    {
        InitializeGrid();
    }

    private void InitializeGrid()
    {
        BlobAssetReference<GridBlobAsset> gridBlobAssetReference;
        var Builder = new BlobBuilder(Allocator.Temp);
        ref GridBlobAsset gridBlobAsset = ref Builder.ConstructRoot<GridBlobAsset>();
        var tilesArray = Builder.Allocate(ref gridBlobAsset.tilesBlobArray, width * height);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                tilesArray[y * width + x] = new Tile
                {
                    position = new int2(x, y),
                    isAvailable = true
                };
            }
        }

        gridBlobAssetReference = Builder.CreateBlobAssetReference<GridBlobAsset>(Allocator.Persistent);
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var girdEntity = entityManager.CreateEntity(ComponentType.ReadOnly<Grid>());
        var girdEntityQuary = entityManager.CreateEntityQuery(ComponentType.ReadOnly<Grid>());

        girdEntityQuary.SetSingleton(new Grid
        {
            width = width,
            height = height,
            gridBlobAssetRef = gridBlobAssetReference
        });

        Builder.Dispose();
    }
}