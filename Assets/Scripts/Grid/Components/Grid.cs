﻿using Unity.Entities;

public struct Grid : IComponentData
{
    public int width;
    public int height;
    public BlobAssetReference<GridBlobAsset> gridBlobAssetRef;
}
