﻿using Unity.Entities;

public struct Attack : IComponentData
{
    public int Value;
}