﻿using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;

public class TurnSystem : ComponentSystem
{
    protected EntityQuery WatingForActionQuery;
    protected EntityQuery InActionQuery;
    protected EntityQuery PlayerTurnQuery;

    protected override void OnCreate()
    {
        PlayerTurnQuery = GetEntityQuery(new EntityQueryDesc()
        {
            All = new ComponentType[] { ComponentType.ReadOnly<Player>(), ComponentType.ReadWrite<AwaitForAction>() },
        });
    }

    protected override void OnUpdate()
    {
        if (IsUnitWaitingForAction() && !IsUnitInAction())
        {
            BeginNextUnitTurn();
        }
    }

    protected void BeginNextUnitTurn()
    {
        var unitsArray = WatingForActionQuery.ToEntityArray(Allocator.TempJob);
        EntityManager.AddComponent(unitsArray.First(), typeof(InAction));
        HighlightUnit(unitsArray.First());
        unitsArray.Dispose();
    }

    private void HighlightUnit(Entity unitEntity)
    {
        var translation = EntityManager.GetComponentData<Translation>(unitEntity);
        EntityManager.AddComponent(unitEntity, typeof(Highlighted));
        EntityManager.SetComponentData(unitEntity, new Highlighted { position = translation.Value });
    }

    public bool IsUnitWaitingForAction()
    {
        return WatingForActionQuery.CalculateEntityCount() > 0;
    }

    public bool IsUnitInAction()
    {
        return InActionQuery.CalculateEntityCount() > 0;
    }

    public bool IsPlayerTurn()
    {
        return PlayerTurnQuery.CalculateEntityCount() > 0;
    }
}