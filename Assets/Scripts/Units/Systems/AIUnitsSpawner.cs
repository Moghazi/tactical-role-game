﻿using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;

public class AIUnitsSpawner : UnitsSpawner
{
    private new void Awake()
    {
        base.Awake();
        var archetype = InitializeAIArchetype();
        SpawnUnits(archetype);
        unitsArray.Dispose();
    }

    private EntityArchetype InitializeAIArchetype()
    {
        var archetype = entityManager.CreateArchetype(
            typeof(Unit),
            typeof(AI),
            typeof(Health),
            typeof(Attack),
            typeof(Translation),
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(RenderBounds),
            typeof(PathPosition),
            typeof(TargetsBuffer)
         );

        return archetype;
    }
}