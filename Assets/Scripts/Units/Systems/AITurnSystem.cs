﻿using Unity.Entities;

public class AITurnSystem : TurnSystem
{
    protected override void OnCreate()
    {
        base.OnCreate();

        WatingForActionQuery = GetEntityQuery(new EntityQueryDesc()
        {
            All = new ComponentType[] { ComponentType.ReadOnly<AI>(), ComponentType.ReadWrite<AwaitForAction>() },
            None = new ComponentType[] { ComponentType.ReadWrite<InAction>() }
        });

        InActionQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[] { ComponentType.ReadOnly<AI>(),
            ComponentType.ReadWrite<AwaitForAction>() ,
            ComponentType.ReadWrite<InAction>() },
        });
    }

    protected override void OnUpdate()
    {
        if (IsUnitWaitingForAction() && !IsUnitInAction() && !IsPlayerTurn())
        {
            BeginNextUnitTurn();
        }
    }
}