﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Units.Systems
{
    public class AIMovementSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithAll<AI, InAction>().WithNone<PathFollow, PathfindingData>().ForEach((Entity entity, ref Translation translation) =>
            {
                var playerPosition = GetNearestPlayerPosition(translation);
                var ValidePosition = GetAvilableNeighborPosition(playerPosition);
                var unitPosX = Mathf.FloorToInt(translation.Value.x);
                var unitPosY = Mathf.FloorToInt(translation.Value.y);

                EntityManager.AddComponent(entity, typeof(Highlighted));
                EntityManager.SetComponentData(entity, new Highlighted
                {
                    position = new float3(ValidePosition.x, ValidePosition.y, 0)
                });

                EntityManager.AddComponentData(entity, new PathfindingData
                {
                    startPosition = new int2(unitPosX, unitPosY),
                    targetPosition = new int2(ValidePosition.x, ValidePosition.y)
                });

                EntityManager.AddComponentData(entity, new PathFollow
                {
                    pathIndex = -1
                });
            });
        }

        private int2 GetNearestPlayerPosition(Translation translation)
        {
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var playerQuery = entityManager.CreateEntityQuery(typeof(Player), typeof(Translation));
            var playersPositionArray = playerQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
            var nearestPosition = playersPositionArray[0].Value;

            for (int i = 0; i < playersPositionArray.Length - 1; i++)
            {
                if (math.distance(translation.Value, playersPositionArray[i].Value) < math.distance(translation.Value, playersPositionArray[i + 1].Value))
                {
                    nearestPosition = playersPositionArray[i].Value;
                }
            }

            var playerPosX = Mathf.FloorToInt(nearestPosition.x);
            var playerPosY = Mathf.FloorToInt(nearestPosition.y);

            playersPositionArray.Dispose();
            return new int2(playerPosX, playerPosY);
        }

        private int2 GetAvilableNeighborPosition(int2 position)
        {
            var neighbourOffsetArray = GridUtilities.GetNeigbourOffsetArray();

            for (int i = 0; i < neighbourOffsetArray.Length; i++)
            {
                int2 neighbourOffset = neighbourOffsetArray[i];
                int2 neighbourPosition = new int2(position.x + neighbourOffset.x, position.y + neighbourOffset.y);

                if (IsValidPosition(neighbourPosition))
                {
                    neighbourOffsetArray.Dispose();
                    return neighbourPosition;
                }
            }

            neighbourOffsetArray.Dispose();
            return new int2(-1, -1);
        }

        private bool IsValidPosition(int2 neighbourPosition)
        {
            var allUnitsQuery = GetEntityQuery(new EntityQueryDesc()
            {
                All = new ComponentType[] { ComponentType.ReadOnly<Unit>(), ComponentType.ReadOnly<Translation>() },
            });

            var allunitsTranslation = allUnitsQuery.ToComponentDataArray<Translation>(Allocator.TempJob);

            var neighbourTranslation = new Translation
            {
                Value = new float3(neighbourPosition.x, neighbourPosition.y, 0)
            };

            foreach (Translation unitTranslation in allunitsTranslation)
            {
                if (math.distance(unitTranslation.Value, neighbourTranslation.Value) < 0.1f)
                {
                    allunitsTranslation.Dispose();
                    return false;
                }
            }

            allunitsTranslation.Dispose();
            return true;
        }
    }
}