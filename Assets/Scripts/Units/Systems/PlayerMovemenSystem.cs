﻿using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;

namespace Units.Systems
{
    public class PlayerMovemenSystem : ComponentSystem
    {
        private float3 startPosition;

        protected override void OnUpdate()
        {
            if (Input.GetMouseButtonDown(0))
            {
                MovePlayer();
            }
        }

        private void MovePlayer()
        {
            var mousePosition = InputUtilities.GetMouseWorldPosition();
            GridUtilities.ConvertToGridPosition(mousePosition, out int targetX, out int targetY);

            Entities.WithAll<Player, InAction>().WithNone<PathFollow, PathfindingData>().ForEach((Entity entity, ref Translation translation) =>
            {
                if (!IsValidePosition(new int2(targetX, targetY)))
                {
                    return;
                }

                GridUtilities.ConvertToGridPosition(translation.Value, out int startPosX, out int startPosY);

                //TODO check if target pos within the grid boundary.
                EntityManager.AddComponentData(entity, new PathfindingData
                {
                    startPosition = new int2(startPosX, startPosY),
                    targetPosition = new int2(targetX, targetY)
                });

                EntityManager.AddComponentData(entity, new Highlighted
                {
                    position = new float3(targetX, targetY, 0)
                });

                EntityManager.AddComponentData(entity, new PathFollow
                {
                    pathIndex = -1
                });
            });
        }

        private bool IsValidePosition(int2 Position)
        {
            var allUnitsQuery = GetEntityQuery(new EntityQueryDesc()
            {
                All = new ComponentType[] { ComponentType.ReadOnly<Unit>(), ComponentType.ReadOnly<Translation>() },
            });

            var allunitsTranslation = allUnitsQuery.ToComponentDataArray<Translation>(Allocator.TempJob);

            var translation = new Translation
            {
                Value = new float3(Position.x, Position.y, 0)
            };

            foreach (Translation unitTranslation in allunitsTranslation)
            {
                if (math.distance(unitTranslation.Value, translation.Value) < 0.1f)
                {
                    allunitsTranslation.Dispose();
                    return false;
                }
            }

            allunitsTranslation.Dispose();
            return true;
        }
    }
}