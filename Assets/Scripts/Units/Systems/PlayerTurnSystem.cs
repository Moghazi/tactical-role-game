﻿using Unity.Entities;

public class PlayerTurnSystem : TurnSystem
{
    protected override void OnCreate()
    {
        WatingForActionQuery = GetEntityQuery(new EntityQueryDesc()
        {
            All = new ComponentType[] { ComponentType.ReadOnly<Player>(), ComponentType.ReadWrite<AwaitForAction>() },
            None = new ComponentType[] { ComponentType.ReadWrite<InAction>() }
        });

        InActionQuery = GetEntityQuery(new EntityQueryDesc()
        {
            All = new ComponentType[] { ComponentType.ReadOnly<Player>(),
            ComponentType.ReadWrite<AwaitForAction>() ,
            ComponentType.ReadWrite<InAction>() }
        });
    }
}