﻿using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;

public class PlayerUnitsSpawner : UnitsSpawner
{
    private new void Awake()
    {
        base.Awake();
        var archtype = InitializePlayerArchetype();
        SpawnUnits(archtype);
        unitsArray.Dispose();
    }

    private EntityArchetype InitializePlayerArchetype()
    {
        var archetype = entityManager.CreateArchetype(
            typeof(Unit),
            typeof(Player),
            typeof(Health),
            typeof(Attack),
            typeof(Translation),
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(RenderBounds),
            typeof(PathPosition),
            typeof(TargetsBuffer)
        );

        return archetype;
    }
}