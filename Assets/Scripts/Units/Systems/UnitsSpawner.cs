﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class UnitsSpawner : MonoBehaviour
{
    [SerializeField]
    private List<UnitData> unitsData;
    protected EntityManager entityManager;
    protected NativeArray<Entity> unitsArray;

    protected void Awake()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        unitsArray = new NativeArray<Entity>(unitsData.Count, Allocator.Temp);
    }

    protected virtual void SpawnUnits(EntityArchetype unitArchetype)
    {
        entityManager.CreateEntity(unitArchetype, unitsArray);

        for (int i = 0; i < unitsData.Count; i++)
        {
            var unit = unitsArray[i];

            entityManager.SetSharedComponentData(unit, new RenderMesh
            {
                mesh = unitsData[i].Mesh,
                material = unitsData[i].Material
            });

            entityManager.SetComponentData(unit, new RenderBounds
            {
                Value = unitsData[i].Mesh.bounds.ToAABB(),
            });

            entityManager.SetComponentData(unit, new Health { Value = unitsData[i].Health });
            entityManager.SetComponentData(unit, new Attack { Value = unitsData[i].Attack });
            entityManager.SetComponentData(unit, new Translation { Value = new float3(unitsData[i].UnitPosition.x, unitsData[i].UnitPosition.y, 0f) });
        }
    }
}