﻿using Unity.Entities;

public class GameLoopSystem : ComponentSystem {

    bool turnIsActive = false;

    protected override void OnUpdate() {
        if (!turnIsActive) {
            BeginTern();
        }

        if (TrunIsEnd()) {
            turnIsActive = false;
        }
    }

    private void BeginTern() {
        Entities.WithAll<Player>().WithNone<AwaitForAction>().ForEach((Entity entity) => {
            EntityManager.AddComponent<AwaitForAction>(entity);
        });

        Entities.WithAll<AI>().WithNone<AwaitForAction>().ForEach((Entity entity) => {
            EntityManager.AddComponent<AwaitForAction>(entity);
        });

        turnIsActive = true;
    }

    private bool TrunIsEnd() {
        var unitsWatingforActionQuery = GetEntityQuery(typeof(AwaitForAction));
        return unitsWatingforActionQuery.CalculateEntityCount() == 0;
    }
}

