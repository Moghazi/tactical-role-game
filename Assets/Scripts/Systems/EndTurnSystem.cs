﻿using Unity.Entities;

public class EndTurnSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.WithAll<EndTurn>().ForEach((Entity entity, DynamicBuffer<PathPosition> pathPositionBuffer) =>
        {
            pathPositionBuffer.Clear();
            EntityManager.RemoveComponent(entity, typeof(ReachedPathTarget));
            EntityManager.RemoveComponent(entity, typeof(InAction));
            EntityManager.RemoveComponent(entity, typeof(AwaitForAction));
            EntityManager.RemoveComponent(entity, typeof(EndTurn));
            EntityManager.RemoveComponent(entity, typeof(PathFollow));
            EntityManager.RemoveComponent(entity, typeof(PathfindingData));
        });
    }
}
