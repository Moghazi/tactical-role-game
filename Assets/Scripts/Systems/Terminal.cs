﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Terminal : MonoBehaviour
{
    [SerializeField]
    private Text text;

    Queue<string> logEntries = new Queue<string>();

    void Awake()
    {
        Application.logMessageReceived += HandleLog;
        StartCoroutine(LogCoroutine());
    }

    public IEnumerator LogCoroutine()
    {
        while (true)
        {
            if (logEntries.Count > 0)
            {
                text.text = logEntries.Dequeue();
            }

            yield return new WaitForSeconds(0.2f);
        }
    }

    string lastLog = "";

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        if (type == LogType.Log)
        {

            if (lastLog != logString)
            {
                logEntries.Enqueue(logString);
                lastLog = logString;
            }
        }
    }
}