﻿using Unity.Collections;
using Unity.Entities;
using UnityEngine;

public class GameOverSystem : ComponentSystem
{
    EntityQuery PlayerUnitsQuery;
    EntityQuery AIUnitsQuery;
    EntityQuery ALLUnitsQuery;

    private bool isGameOver = false;

    protected override void OnCreate()
    {
        PlayerUnitsQuery = GetEntityQuery(new EntityQueryDesc()
        {
            All = new ComponentType[] { ComponentType.ReadOnly<Player>() },
        });

        AIUnitsQuery = GetEntityQuery(new EntityQueryDesc()
        {
            All = new ComponentType[] { ComponentType.ReadOnly<AI>() },
        });

        ALLUnitsQuery = GetEntityQuery(new EntityQueryDesc()
        {
            All = new ComponentType[] { ComponentType.ReadOnly<Unit>() },
        });
    }

    protected override void OnUpdate()
    {
       if(PlayerUnitsQuery.CalculateEntityCount() == 0 && !isGameOver)
        {
            isGameOver = true;
            Debug.Log("GAME OVER, Player Lose...");

            DestroyAllUnits();
        }

        if (AIUnitsQuery.CalculateEntityCount() == 0 && !isGameOver)
        {
            isGameOver = true;

            Debug.Log("GAME OVER, Player WON...");

            DestroyAllUnits();
        }
    }

    private void DestroyAllUnits()
    {
        NativeArray<Entity> allEntites = ALLUnitsQuery.ToEntityArray(Allocator.TempJob);
        foreach (Entity entity in allEntites)
        {
            EntityManager.DestroyEntity(entity);
        }
        allEntites.Dispose();
    }
}